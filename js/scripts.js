
var windowWidth, h_hght, map_scale;
var scrollPosition = null;

$(document).ready(function() {
	
	/* slider */
	if($('#info-slider')[0]){
		
		$('.b-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			dots: true,
			appendArrows: '.b-slider__nav',
			appendDots: '.b-slider__nav',
			fade: true
		});
		$('.b-slider').on('beforeChange', function(event, slick, currentSlide){
			new WOW().init();
		});
		
	}
	//--
	
	
	/* mobile menu */
	$(document).click( function(event){
		if( $(event.target).closest('header').length ) 
		return;
		$('body').removeClass('menu-toggle');
		event.stopPropagation();
	});
	$('.mobile-menu-toggle').click(function(){
		
		if (!$('body').hasClass('menu-toggle')) {
			scrollPosition = $(document).scrollTop();
			$('body').toggleClass('menu-toggle').css('top', '-'+scrollPosition+'px');
		} else {
			$('body').removeClass('menu-toggle');
			$(document).scrollTop(scrollPosition);
		}
		
	});
	//--
	
	
	/* animations */
	new WOW().init();
	//--
	
	
	/* scroll to second screen */
	$('#scrollto').click(function(e) {
		
		e.preventDefault();
		checkScreen();
		
		var target = $('#about').offset().top - h_hght;
		
		$(window).stop(true).scrollTo({left:0, top:target}, {duration:800, interrupt:true});
		
	});
	//--
	
	
	/* google map */
	if($('#map')[0]){
		
		google.maps.event.addDomListener(window, 'load', init);
		google.maps.event.addDomListener(window, "resize", init);		
		
		var markerSize = { x: 10, y: 50 };	
		
		google.maps.Marker.prototype.setLabel = function(label){
				this.label = new MarkerLabel({
					map: this.map,
					marker: this,
					text: label
				});
				this.label.bindTo('position', this, 'position');
		};

		var MarkerLabel = function(options) {
				this.setValues(options);
				this.span = document.createElement('span');
				this.span.className = 'map_label';
		};

		MarkerLabel.prototype = $.extend(new google.maps.OverlayView(), {
				onAdd: function() {
						this.getPanes().overlayImage.appendChild(this.span);
						var self = this;
						this.listeners = [
						google.maps.event.addListener(this, 'position_changed', function() { self.draw();    })];
				},
				draw: function() {
						var text = String(this.get('text'));
						var position = this.getProjection().fromLatLngToDivPixel(this.get('position'));
						this.span.innerHTML = text;
						this.span.style.left = (position.x - 22 + (markerSize.x / 2)) + (text.length) - 10 + 'px';
						this.span.style.top = (position.y - markerSize.y + 29) + 'px';
				}
		});
		
		onResize();
		
		
		var markerIcon = {
			url: 'img/marker.png',
		}
				
		function init() {
				var mapOptions = {
						zoom: map_scale,
						scrollwheel: false,
						center: new google.maps.LatLng(55.775082, 37.613999),
						disableDefaultUI: true,
						styles: [
						{
								"featureType": "all",
								"elementType": "labels.text",
								"stylers": [
										{
												"visibility": "off"
										}
								]
						},
						{
								"featureType": "all",
								"elementType": "labels.text.fill",
								"stylers": [
										{
												"saturation": 36
										},
										{
												"color": "#000000"
										},
										{
												"lightness": 40
										}
								]
						},
						{
								"featureType": "all",
								"elementType": "labels.text.stroke",
								"stylers": [
										{
												"visibility": "on"
										},
										{
												"color": "#000000"
										},
										{
												"lightness": 16
										}
								]
						},
						{
								"featureType": "all",
								"elementType": "labels.icon",
								"stylers": [
										{
												"visibility": "off"
										}
								]
						},
						{
								"featureType": "administrative",
								"elementType": "geometry.fill",
								"stylers": [
										{
												"color": "#000000"
										},
										{
												"lightness": 20
										}
								]
						},
						{
								"featureType": "administrative",
								"elementType": "geometry.stroke",
								"stylers": [
										{
												"color": "#000000"
										},
										{
												"lightness": 17
										},
										{
												"weight": 1.2
										}
								]
						},
						{
								"featureType": "administrative",
								"elementType": "labels.text",
								"stylers": [
										{
												"visibility": "off"
										}
								]
						},
						{
								"featureType": "administrative.country",
								"elementType": "labels.text",
								"stylers": [
										{
												"visibility": "off"
										}
								]
						},
						{
								"featureType": "administrative.province",
								"elementType": "labels.text",
								"stylers": [
										{
												"visibility": "off"
										}
								]
						},
						{
								"featureType": "administrative.locality",
								"elementType": "labels.text",
								"stylers": [
										{
												"visibility": "off"
										}
								]
						},
						{
								"featureType": "landscape",
								"elementType": "geometry",
								"stylers": [
										{
												"color": "#1e2834"
										},
										{
												"lightness": "1"
										}
								]
						},
						{
								"featureType": "landscape.natural",
								"elementType": "labels.text",
								"stylers": [
										{
												"visibility": "off"
										}
								]
						},
						{
								"featureType": "poi",
								"elementType": "geometry",
								"stylers": [
										{
												"color": "#000000"
										},
										{
												"lightness": 21
										}
								]
						},
						{
								"featureType": "poi",
								"elementType": "geometry.fill",
								"stylers": [
										{
												"color": "#1e2834"
										}
								]
						},
						{
								"featureType": "poi",
								"elementType": "labels.text",
								"stylers": [
										{
												"visibility": "off"
										}
								]
						},
						{
								"featureType": "road",
								"elementType": "geometry.fill",
								"stylers": [
										{
												"color": "#16202c"
										}
								]
						},
						{
								"featureType": "road",
								"elementType": "geometry.stroke",
								"stylers": [
										{
												"visibility": "on"
										},
										{
												"color": "#16202c"
										}
								]
						},
						{
								"featureType": "road",
								"elementType": "labels.text",
								"stylers": [
										{
												"visibility": "off"
										}
								]
						},
						{
								"featureType": "road.highway",
								"elementType": "geometry.fill",
								"stylers": [
										{
												"color": "#121c28"
										},
										{
												"lightness": "0"
										}
								]
						},
						{
								"featureType": "road.highway",
								"elementType": "geometry.stroke",
								"stylers": [
										{
												"color": "#26303c"
										},
										{
												"lightness": "0"
										},
										{
												"weight": 0.2
										}
								]
						},
						{
								"featureType": "road.highway.controlled_access",
								"elementType": "geometry.fill",
								"stylers": [
										{
												"color": "#16202c"
										},
										{
												"visibility": "on"
										}
								]
						},
						{
								"featureType": "road.arterial",
								"elementType": "geometry",
								"stylers": [
										{
												"color": "#000000"
										},
										{
												"lightness": 18
										}
								]
						},
						{
								"featureType": "road.arterial",
								"elementType": "geometry.fill",
								"stylers": [
										{
												"color": "#121c28"
										}
								]
						},
						{
								"featureType": "road.arterial",
								"elementType": "geometry.stroke",
								"stylers": [
										{
												"color": "#26303c"
										},
										{
												"gamma": "0.52"
										}
								]
						},
						{
								"featureType": "road.local",
								"elementType": "geometry",
								"stylers": [
										{
												"color": "#000000"
										},
										{
												"lightness": 16
										}
								]
						},
						{
								"featureType": "road.local",
								"elementType": "geometry.fill",
								"stylers": [
										{
												"color": "#141d29"
										}
								]
						},
						{
								"featureType": "road.local",
								"elementType": "geometry.stroke",
								"stylers": [
										{
												"color": "#16202c"
										},
										{
												"visibility": "on"
										}
								]
						},
						{
								"featureType": "transit",
								"elementType": "all",
								"stylers": [
										{
												"visibility": "on"
										},
										{
												"color": "#151f2b"
										}
								]
						},
						{
								"featureType": "transit",
								"elementType": "geometry",
								"stylers": [
										{
												"color": "#000000"
										},
										{
												"lightness": 19
										}
								]
						},
						{
								"featureType": "transit",
								"elementType": "geometry.fill",
								"stylers": [
										{
												"color": "#151f2b"
										}
								]
						},
						{
								"featureType": "transit",
								"elementType": "labels.text",
								"stylers": [
										{
												"visibility": "off"
										}
								]
						},
						{
								"featureType": "water",
								"elementType": "geometry",
								"stylers": [
										{
												"color": "#000000"
										},
										{
												"lightness": 17
										},
										{
												"visibility": "on"
										}
								]
						},
						{
								"featureType": "water",
								"elementType": "geometry.fill",
								"stylers": [
										{
												"color": "#17212d"
										},
										{
												"visibility": "on"
										}
								]
						}
				]
				};
				var mapElement = document.getElementById('map');
				var map = new google.maps.Map(mapElement, mapOptions);
				var marker = new google.maps.Marker({
						position: new google.maps.LatLng(55.761482, 37.584183),
						map: map,
						icon: markerIcon,
						label: 'Россия, г. Москва, <br>ул. Садовая-Кудринская, д. 7, стр. 13'
				});	
				var marker2 = new google.maps.Marker({
						position: new google.maps.LatLng(55.787276, 37.622839),
						map: map,
						icon: markerIcon,
						label: 'Россия, г. Москва, Олимпийский проспект 22, Москва'
				});	
				
			
		}
		
	}
	//--
	
	
	$(window).resize(onResize);
	onResize();	
	
});


function onResize(){
	
	windowWidth = $(window).width();
	
	/* google map scale */
	if(windowWidth < 500)
		map_scale = 12; // for mobile
	else
		map_scale = 13; // for other
	//--
	
	
	if (!$('body').hasClass('menu-toggle')) {
		scrollPosition = $(document).scrollTop();
	} else {
		$(document).scrollTop(scrollPosition);
	}
	
}


/* fixed header */
$(function(){
	$(window).scroll(function(){
		
		var top = $(this).scrollTop() + h_hght;
		var elem = $('.b-header__top');
		
		checkScreen();
			
		if (top <= h_hght) {
		 elem.removeClass('fixed');
		 $('.b-header__bg').css('padding-top', '0px');
		} else {
		 elem.addClass('fixed');
		 $('.b-header__bg').css('padding-top', h_hght+'px');
		}
		
	});
});
//--


/* check screen width */
function checkScreen(){
	if(windowWidth <= 1230 && windowWidth > 760)
		h_hght = 60;
	else if (windowWidth <= 760)
		h_hght = 44;
	else
		h_hght = 103;
}
//--
