
$(document).ready(function() {
	
	$('.services-list__link').click(function(e){

    	href = $(this).attr('href');

    	$(this).parents('.services-list').find('.services-list__link').each(function(){
    		$(this).removeClass('active');
    	});
    	$(this).addClass('active');

    	$(this).parents('.main-content-wrapper').find('.tabs-item').each(function(){
    		
    		var idTab = '#' + $(this).attr('id');
    		if( idTab == href ){
    			$(this).siblings().removeClass('active');
    			$(this).addClass('active');
    		}
    	})
    	e.preventDefault();
    });

    $('.tabs-item__link').click(function(e){
    	
    	$(this).parents('.tabs-wrapper').find('.tabs-item__link').each(function(){
    		$(this).removeClass('active');
    	});
    	$(this).addClass('active');
    	$(this).parents('.tabs-item').siblings().removeClass('active');
    	$(this).parents('.tabs-item').addClass('active');
    })
		
});